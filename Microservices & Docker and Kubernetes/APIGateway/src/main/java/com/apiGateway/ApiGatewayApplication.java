package com.apiGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	
	@Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("user-router",r -> r.path("/users/**")
                        .uri("http://localhost:9002/")
                        )

                .route("product-router" ,r -> r.path("/products/**")
                        .uri("http://localhost:9001/")
                        )
                
                .route("cart-router" ,r -> r.path("/cart/**")
                        .uri("http://localhost:9003/")
                        )
                .build();
    }

}
