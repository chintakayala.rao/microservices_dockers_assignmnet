package com.user.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.user.exceptionHandling.InputException;
import com.user.model.User;
import com.user.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService service;

	@GetMapping("/users/register")
	public ResponseEntity<User> register(@Valid @RequestBody User user) throws Exception {
		String userEmailId = user.getEmailId();

		if (userEmailId != null && !"".equals(userEmailId)) {
			User userobj = service.fetchUserByEmailId(userEmailId);
			if (userobj != null) {
				throw new Exception("User with " + userEmailId + " already exist");
			}
		}
		User userobj = service.saveUser(user);
		return new ResponseEntity<User>(userobj, HttpStatus.OK);
	}

	@GetMapping("/users/")
	public ResponseEntity<List<User>> getProduct() {
		List<User> userList = service.listAll();

		if (userList.size() <= 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.of(Optional.of(userList));
	}

	@PostMapping("/users/login")
	public User loginUser(@RequestBody User user) {
		String userEmailId = user.getEmailId();
		String userpass = user.getPassword();

		User userobj = null;
		if (userEmailId != null && userpass != null) {
			userobj = service.fetchUserByEmailIdAndPassword(userEmailId, userpass);
		}
		if (userEmailId == null && userpass == null) {
			throw new InputException("500", "Please fill the fields");
		}
		if (userobj == null) {
			throw new InputException("400", "Email or password is incorrect");
		}
		return userobj;
	}

}
