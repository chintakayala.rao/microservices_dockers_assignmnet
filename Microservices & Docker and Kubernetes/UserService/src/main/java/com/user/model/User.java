package com.user.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer uid;
	
	@NotEmpty(message="Enter the full name!")
	private String fullName;

	@Email(message="Please enter a valid email id!")
	private String emailId;
	
	@NotEmpty
	@Size(min=3, max=5, message="Password must be min char 3 and max char 10 !!")
	private String password;
	
	@NotEmpty(message="Role can be user or admin")
	private String role;
	
	public User(Integer uid, String fullName, String emailId, String password, String role) {
		super();
		this.uid = uid;
		this.fullName = fullName;
		this.emailId = emailId;
		this.password = password;
		this.role = role;
	}

	public User() {
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
