package com.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.product.model.Product;
import com.product.repository.AddProductRepository;

@Service
public class AddProductService {

	@Autowired
	private AddProductRepository repo;
	
	public Product addNewProduct(Product Product){
		return repo.save(Product);
	}
	
	public void save(Product product) {
        repo.save(product);
    }
	
	public Product get(Integer productId) {
        return repo.findById(productId).get();
    } 
	
	public List<Product> listAll() {
        return repo.findAll();
    }

	public void delete(Integer productId) {
        repo.deleteById(productId);
    }
}
