package com.product.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.product.model.Product;
import com.product.service.AddProductService;

@RestController
public class AddProductController {
	@Autowired
	private AddProductService productService;

	@GetMapping("/products/")
	public ResponseEntity<List<Product>> getProduct() {

		List<Product> productList = productService.listAll();

		if (productList.size() <= 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.of(Optional.of(productList));
	}

	@GetMapping("/products/{id}")
	public ResponseEntity<Product> get(@PathVariable Integer id) {
		try {
			Product product = productService.get(id);
			return new ResponseEntity<Product>(product, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/products/add")
	public ResponseEntity<Product> addNewProduct(@Valid @RequestBody Product proudct) {
		Product prodobj = null;
		try {
			prodobj = this.productService.addNewProduct(proudct);
			return ResponseEntity.of(Optional.of(prodobj));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@PutMapping("/products/{productId}")
	public ResponseEntity<?> updateProduct(@RequestBody Product product, @PathVariable Integer productId) {
		try {
			Product existProduct = productService.get(productId);
			productService.save(product);
			return new ResponseEntity<>(existProduct, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/products/{productId}")
	public ResponseEntity<Void> delete(@PathVariable Integer productId) {
		try {
			this.productService.delete(productId);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
